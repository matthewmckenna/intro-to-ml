{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/Users/matthewmckenna/miniconda3/envs/intro-to-ml/lib/python3.7/site-packages/sklearn/utils/__init__.py:4: DeprecationWarning: Using or importing the ABCs from 'collections' instead of from 'collections.abc' is deprecated, and in 3.8 it will stop working\n",
      "  from collections import Sequence\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Python version: 3.7.0 (default, Jun 28 2018, 07:39:16) \n",
      "[Clang 4.0.1 (tags/RELEASE_401/final)]\n",
      "pandas version: 0.23.4\n",
      "matplotlib version: 2.2.3\n",
      "NumPy version: 1.15.1\n",
      "SciPy version: 1.1.0\n",
      "IPython version: 6.4.0\n",
      "scikit-learn version: 0.19.1\n"
     ]
    }
   ],
   "source": [
    "%matplotlib inline\n",
    "import sys\n",
    "import pandas as pd\n",
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import scipy as sp\n",
    "import IPython\n",
    "import sklearn\n",
    "from IPython.display import display\n",
    "import mglearn\n",
    "\n",
    "print(f'Python version: {sys.version}')\n",
    "print(f'pandas version: {pd.__version__}')\n",
    "print(f'matplotlib version: {matplotlib.__version__}')\n",
    "print(f'NumPy version: {np.__version__}')\n",
    "print(f'SciPy version: {sp.__version__}')\n",
    "print(f'IPython version: {IPython.__version__}')\n",
    "print(f'scikit-learn version: {sklearn.__version__}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Naive Bayes Classifiers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Naive Bayes classifiers are a family of classifiers that are quite similar to the linear models discussed in the previous section. However, they tend to be even faster in training. The price paid is that the generalisation performance is slightly worse than that of linear classifiers.\n",
    "\n",
    "The reason naive Bayes models are so efficient is that they learn parameters by looking at each feature individually, and collect simple per-class statistics from each feature.\n",
    "\n",
    "There are three kinds of naive Bayes classifier implemented in scikit-learn:\n",
    "\n",
    "1. GaussianNB\n",
    "2. BernoulliNB\n",
    "3. MultinomialNB\n",
    "\n",
    "`GaussianNB` can be applied to any continuous data, while `BernoulliNB` assumes binary data and `MultinomialNB` assumes count data (such as how many times a word appears in a sentence).\n",
    "\n",
    "`BernoulliNB` and `MultinomialNB` are mostly used in text data classification.\n",
    "\n",
    "The `BernoulliNB` classifier counts how often every feature of each class is not zero."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = np.array(\n",
    "    [\n",
    "        [0, 1, 0, 1],\n",
    "        [1, 0, 1, 1],\n",
    "        [0, 0, 0, 1],\n",
    "        [1, 0, 1, 0],\n",
    "    ],\n",
    ")\n",
    "y = np.array([0, 1, 0, 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have four data points, with four binary features each.\n",
    "\n",
    "There are two classes: 0 and 1.\n",
    "\n",
    "For class 0 (the first and third data points), the first feature is zero two times, and nonzero zero times.\n",
    "The second feature is zero one time, and nonzero one time.\n",
    "\n",
    "The same counts are calculated for the data points in the second class.\n",
    "Counting the nonzero entries per class in essence looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Feature counts:\n",
      "{0: array([0, 1, 0, 2]), 1: array([2, 0, 2, 1])})\n"
     ]
    }
   ],
   "source": [
    "counts = {}\n",
    "for label in np.unique(y):\n",
    "    # iterate over each class\n",
    "    # count (sum) entries of 1 per feature\n",
    "    counts[label] = X[y == label].sum(axis=0)\n",
    "print(f'Feature counts:\\n{counts})')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The other two naive Bayes models are slightly different in what kinds of statistics they compute.\n",
    "\n",
    "`MultinomialNB` take into account the average value of each feature for each class, while `GaussianNB` stores the average value as well as the standard deviation of each feature for each class.\n",
    "\n",
    "To make a prediction, a data point is compared to the statistics for each of the classes, and the best matching class is predicted.\n",
    "\n",
    "Unfortunately, `coef_` for the naive Bayes models has a somewhat different meaning than in the linear models, in that `coef_` is not the same as `w`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Strengths, Weaknesses, and Parameters\n",
    "\n",
    "Parameters\n",
    "\n",
    "* `MultinomialNB` and `BernoulliNB` have a single parameter `alpha` which controls model complexity\n",
    "  * The algorithm adds to the data `alpha` many virtual data points that have positive values for all the features\n",
    "  * Results in a smoothing of the statistics\n",
    "  * Large `alpha` means more smoothing, resulting in less complex models\n",
    "  * Algorithm's performance relatively robust to the setting of `alpha` meaning that setting this is not critical for good performance\n",
    "  * However, tuning it usually improves accuracy somewhat\n",
    "  \n",
    "* `GaussianNB` is mostly used on very high-dimensional data, while the other two variants are widely used for sparse count data such as text.\n",
    "* `MultinomialNB` usually performs better than `BernoulliNB` particularly on datasets with a relatively large number of nonzero features (i.e., large documents)\n",
    "\n",
    "* Naive Bayes models share many of the strengths and weaknesses of the linear models.\n",
    "* Very fast to train and predict, and training procedure easy to understand.\n",
    "* Models work well with high-dimensional sparse data and are relatively robust to the parameters.\n",
    "* Great baseline models and often used on very large datasets, where training even a linear model might take too long."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
